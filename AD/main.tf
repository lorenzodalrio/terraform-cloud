resource "aws_directory_service_directory" "test-petronas" {
  name     = "test.clouddevops.eng.it"
  password = "SuperSecretPassw0rd"
  edition  = "Standard"
  type     = "MicrosoftAD"

  vpc_settings {
    vpc_id     = var.vpc-id
    subnet_ids = [var.subnet1-id, var.subnet2-id]
  }

  tags = {
    Project = "petronas-track-trace"
  }
}