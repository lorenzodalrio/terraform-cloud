terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-1"
}

module "AD" {
  source     = "./AD/"
  vpc-id     = module.network.vpc_id
  subnet1-id = module.network.public_subnets[0]
  subnet2-id = module.network.public_subnets[1]
}

module "network" {
  source               = "terraform-aws-modules/vpc/aws"
  version              = "2.62.0"
  name                 = "test-petronas"
  cidr                 = "10.0.0.0/16"
  azs                  = ["eu-west-1a", "eu-west-1b"]
  public_subnets       = ["10.0.0.0/22", "10.0.4.0/22"]
  private_subnets      = []
  enable_dns_support   = true
  enable_dns_hostnames = true
  enable_nat_gateway   = false
  tags = {
    Name = "test-petronas"
  }
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

module "key-pair" {
  source     = "terraform-aws-modules/key-pair/aws"
  version    = "0.5.0"
  key_name   = "petronas-test-keypair"
  public_key = tls_private_key.this.public_key_openssh
}

module "security-group_rdp" {
  source              = "terraform-aws-modules/security-group/aws//modules/rdp"
  version             = "3.16.0"
  name                = "bastion-host-sg"
  vpc_id              = module.network.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "bastion-host" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "bastion-host"
  instance_count = 1

  ami                         = "ami-010f54206a0172cec"
  instance_type               = "t2.micro"
  key_name                    = module.key-pair.this_key_pair_key_name
  monitoring                  = true
  vpc_security_group_ids      = [module.security-group_rdp.this_security_group_id]
  subnet_id                   = module.network.public_subnets[0]
  associate_public_ip_address = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
    Customer    = "Petronas"
  }
}