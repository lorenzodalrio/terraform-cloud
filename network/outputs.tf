output "vpc-id" {
    value = aws_vpc.main.id
}

output "subnet1-id" {
    value = aws_subnet.subnet1.id
}

output "subnet2-id" {
    value = aws_subnet.subnet2.id
}